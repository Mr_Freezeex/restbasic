from flask import Flask, jsonify

app = Flask(__name__)

@app.route("/")
def index():
    return "Hello DevOps"

@app.route("/hellojson")
def hello():
    return jsonify(hello="DevOps")

@app.route("/woof")
def woof():
    return "woof", 500
