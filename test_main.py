import unittest
import main
from flask import json, jsonify

class TestMain(unittest.TestCase):
    def setUp(self):
        self.app = main.app.test_client()

    def tearDown(self):
        pass

    def test_index(self):
        resp = self.app.get('/')
        assert b"Hello DevOps" == resp.data

    def test_hello(self):
        resp = self.app.get('/hellojson')
        data = json.loads(resp.data)
        assert "DevOps" ==  data["hello"]

    def test_woof_text(self):
        resp = self.app.get("/woof")
        assert b"woof" == resp.data

    def test_woof_status(self):
        resp = self.app.get("/woof")
        assert 500 == resp.status_code
